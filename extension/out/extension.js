"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
// eslint-disable-next-line no-unused-vars
const vscode_languageclient_1 = require("vscode-languageclient");
const com = require("./commands");
const utils_1 = require("./utils");
let langClient;
function deactivate() {
    if (!langClient) {
        return undefined;
    }
    return langClient.stop();
}
exports.deactivate = deactivate;
function setupLSP() {
    utils_1.withRacket((racket) => {
        const executable = {
            command: racket,
            args: ["--lib", "racket-langserver"],
        };
        // If the extension is launched in debug mode then the debug server options are used
        // Otherwise the run options are used
        const serverOptions = {
            run: executable,
            debug: executable,
        };
        // Options to control the language client
        const clientOptions = {
            // Register the server for racket documents
            documentSelector: [{ scheme: "file", language: "racket" }],
            synchronize: {
                // Notify the server about file changes to '.clientrc files contained in the workspace
                fileEvents: vscode.workspace.createFileSystemWatcher("**/.clientrc"),
            },
            uriConverters: {
                code2Protocol: (uri) => uri.toString(true),
                protocol2Code: (str) => vscode.Uri.parse(str),
            },
        };
        // Create the language client and start the client.
        langClient = new vscode_languageclient_1.LanguageClient("magic-racket", "Racket Language Client", serverOptions, clientOptions);
        // Start the client. This will also launch the server
        langClient.start();
    });
}
function reg(name, func) {
    return vscode.commands.registerCommand(`magic-racket.${name}`, func);
}
function activate(context) {
    setupLSP();
    // Each file has one output terminal and one repl
    // Those two are saved in terminals and repls, respectively
    // The file is _ran_ in the terminal and _loaded_ into a repl
    const terminals = new Map();
    const repls = new Map();
    vscode.window.onDidCloseTerminal((terminal) => {
        terminals.forEach((val, key) => val === terminal && terminals.delete(key) && val.dispose());
        repls.forEach((val, key) => val === terminal && repls.delete(key) && val.dispose());
    });
    const loadInRepl = reg("loadFileInRepl", () => com.loadInRepl(repls));
    const runInTerminal = reg("runFile", () => com.runInTerminal(terminals));
    const executeSelection = reg("executeSelectionInRepl", () => com.executeSelection(repls));
    const openRepl = reg("openRepl", () => com.openRepl(repls));
    const showOutput = reg("showOutputTerminal", () => com.showOutput(terminals));
    context.subscriptions.push(loadInRepl, runInTerminal, executeSelection, openRepl, showOutput);
}
exports.activate = activate;
//# sourceMappingURL=extension.js.map