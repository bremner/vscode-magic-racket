"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
function fileName(filePath) {
    const match = filePath.match(/^.*\/([^/]+\.[^/]+)$/);
    if (match) {
        return match[1];
    }
    vscode.window.showErrorMessage("Invalid file name.");
    return "";
}
function executeSelectionInRepl(repl, editor) {
    editor.selections.forEach((sel) => {
        const trimmed = editor.document.getText(sel).trim();
        if (trimmed) {
            repl.show();
            repl.sendText(trimmed);
        }
    });
}
exports.executeSelectionInRepl = executeSelectionInRepl;
function runFileInTerminal(racket, filePath, terminal) {
    terminal.show();
    const shell = vscode.workspace
        .getConfiguration("terminal.integrated.shell")
        .get("windows");
    if (process.platform === "win32" && shell && /cmd\.exe$/.test(shell)) {
        // cmd.exe doesn't recognize single quotes
        terminal.sendText(`${racket} "${filePath}"`);
    }
    else {
        terminal.sendText(`${racket} '${filePath}'`);
    }
}
exports.runFileInTerminal = runFileInTerminal;
function loadFileInRepl(filePath, repl) {
    repl.show();
    repl.sendText(`(enter! (file "${filePath}"))`);
}
exports.loadFileInRepl = loadFileInRepl;
function createTerminal(filePath) {
    let terminal;
    if (filePath) {
        const templateSetting = vscode.workspace
            .getConfiguration("magic-racket.outputTerminal")
            .get("outputTerminalTitle");
        const template = templateSetting && templateSetting !== "" ? templateSetting : "Output ($name)";
        terminal = vscode.window.createTerminal(template.replace("$name", fileName(filePath)));
    }
    else {
        const templateSetting = vscode.workspace
            .getConfiguration("magic-racket.outputTerminal")
            .get("sharedOutputTerminalTitle");
        const template = templateSetting && templateSetting !== "" ? templateSetting : "Racket Output";
        terminal = vscode.window.createTerminal(template);
    }
    terminal.show();
    return terminal;
}
exports.createTerminal = createTerminal;
function createRepl(filePath, racket) {
    const templateSetting = vscode.workspace
        .getConfiguration("magic-racket.repl")
        .get("replTitle");
    const template = templateSetting && templateSetting !== "" ? templateSetting : "REPL ($name)";
    const repl = vscode.window.createTerminal(template.replace("$name", fileName(filePath)));
    repl.show();
    repl.sendText(racket);
    return repl;
}
exports.createRepl = createRepl;
//# sourceMappingURL=repl.js.map