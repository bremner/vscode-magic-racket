"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
function normalizeFilePath(filePath) {
    if (process.platform === "win32") {
        return filePath.replace(/\\/g, "/");
    }
    return filePath;
}
function withRacket(func) {
    const racket = vscode.workspace.getConfiguration("magic-racket.general").get("racketPath");
    if (racket !== "") {
        func(racket);
    }
    else {
        vscode.window.showErrorMessage("No Racket executable specified. Please add the path to the Racket executable in settings");
    }
}
exports.withRacket = withRacket;
function withEditor(func) {
    const editor = vscode.window.activeTextEditor;
    if (editor) {
        func(editor);
    }
    else {
        vscode.window.showErrorMessage("A file must be opened before you can do that");
    }
}
exports.withEditor = withEditor;
function withFilePath(func) {
    withEditor((editor) => func(normalizeFilePath(editor.document.fileName)));
}
exports.withFilePath = withFilePath;
//# sourceMappingURL=utils.js.map