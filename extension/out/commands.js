"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const repl_1 = require("./repl");
const utils_1 = require("./utils");
function getOrDefault(map, key, getDefault) {
    if (!map.has(key)) {
        map.set(key, getDefault());
    }
    return map.get(key);
}
function runInTerminal(terminals) {
    utils_1.withFilePath((filePath) => {
        utils_1.withRacket((racket) => {
            let terminal;
            if (vscode.workspace
                .getConfiguration("magic-racket.outputTerminal")
                .get("numberOfOutputTerminals") === "one") {
                terminal = getOrDefault(terminals, "one", () => repl_1.createTerminal(null));
            }
            else {
                terminal = getOrDefault(terminals, filePath, () => repl_1.createTerminal(filePath));
            }
            repl_1.runFileInTerminal(racket, filePath, terminal);
        });
    });
}
exports.runInTerminal = runInTerminal;
function loadInRepl(repls) {
    utils_1.withFilePath((filePath) => {
        utils_1.withRacket((racket) => {
            const repl = getOrDefault(repls, filePath, () => repl_1.createRepl(filePath, racket));
            repl_1.loadFileInRepl(filePath, repl);
        });
    });
}
exports.loadInRepl = loadInRepl;
function executeSelection(repls) {
    utils_1.withEditor((editor) => {
        utils_1.withFilePath((filePath) => {
            utils_1.withRacket((racket) => {
                const repl = getOrDefault(repls, filePath, () => repl_1.createRepl(filePath, racket));
                repl_1.executeSelectionInRepl(repl, editor);
            });
        });
    });
}
exports.executeSelection = executeSelection;
function openRepl(repls) {
    utils_1.withFilePath((filePath) => {
        utils_1.withRacket((racket) => {
            const repl = getOrDefault(repls, filePath, () => repl_1.createRepl(filePath, racket));
            repl.show();
        });
    });
}
exports.openRepl = openRepl;
function showOutput(terminals) {
    utils_1.withFilePath((filePath) => {
        const terminal = terminals.get(filePath);
        if (terminal) {
            terminal.show();
        }
        else {
            vscode.window.showErrorMessage("No output terminal exists for this file");
        }
    });
}
exports.showOutput = showOutput;
//# sourceMappingURL=commands.js.map